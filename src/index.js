//iframe控制
          /*function changeSrc()
          {
            $('iframe').attr('src','https://www.baidu.com')
          }*/
         import Vue from 'vue'
          Vue.component('menu-item', {
            // todo-item 组件现在接受一个s
            // "prop"，类似于一个自定义特性。
            // 这个 prop 名为 todo。
            props: ['menu'],
            template: '<a class="item"><i v-bind:class="menu.icon"></i> {{menu.text}} </a>'
          })

          var app7 = new Vue({
            el: '#menuapp',
            data: {
              menuList: [
                { id: 0, text: '蔬菜',icon: 'grid layout icon' },
                { id: 1, text: '奶酪',icon: 'mail icon' }
              ]
            }
          })
          var dataSource = {
            message:'hello'
          };
          var inputapp = new Vue({
            el: '#inputapp',
            data: dataSource,
            methods:{
              process(event){
                dataSource.message = event.target.value;
                console.log(event.target.value)
              }
            }
          })
          var checkboxdata = {
            message:[]
          };
          var checkboxapp = new Vue({
            el: '#checkboxapp',
            data: checkboxdata,
            methods:{
              process(event){
                dataSource.message = event.target.value;
                console.log(event.target.value)
              }
            }
          })

          var radiodata = {
            message:[]
          };
          var radioapp = new Vue({
            el: '#radioapp',
            data: radiodata,
            methods:{
              process(event){
                dataSource.message = event.target.value;
                console.log(event.target.value)
              }
            }
          })

          var selectdata = {
            selected:'',
            options:[{value:'hleeo'},{value:'hola'},{value:'hhaa'}]
          };
          var selectapp = new Vue({
            el: '#selectapp',
            data: selectdata
          })

          var app = new Vue({
          el: '#app',
          data: {
            message: 'Hello Vue!'
          }
        })

        var app2 = new Vue({
          el: '#app-2',
          data: {
            message: '页面加载于 ' + new Date().toLocaleString()
          }
        })
        function change(k){
          app.message=k;
        }
        $('#d').dropdown();
        /**
         * vue组件
         */
        Vue.component('ui-button',{
          template: '<button class="ui button"> {{ btntext }} </button>',
          props:{
            btntext:{
              type:String,
              default: 'button',
              required:true,
              validator(value){
                return value.length>3
              }
            }
          }
        })

        Vue.component('ui-counterbutton',{
          template: '<button @click="increment"  class="ui button"> {{ counter }} </button>',
          data(){
            return {
              counter: 0
            }
          },
          methods:{
            increment(){
              this.counter += 1,
              this.$emit('increment')
            }
          }
        })
        var cmpdatasrouce = {
            oked: false,
            total: 0
        }
        Vue.component('segment',{
          template:'\
            <div class="ui stacked segment"> \
            <slot>:)</slot>\
            </div>\
          '
        })
        Vue.component('card',{
          template:'\
            <div class="ui card"> \
              <div class="image">\
                <slot name="image">image</slot>\
              </div>\
              <div class="content">\
                <div class="header">\
                  <slot name="header">header</slot>\
                </div>\
                <div class="meta">\
                  <slot name="meta">meta</slot>\
                </div>\
              </div>\
            </div>\
          '
        })
        var componentapp = new Vue({
          el:'#componentapp',
          data:cmpdatasrouce,
          components:{
          
          },
          methods:{
            incrementTotal(){
              this.total += 1
            }
          }
        })
        