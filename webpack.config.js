const path = require('path');

module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist')
  },    
  devServer: {
    historyApiFallback: true,
    overlay: true
  },
  resolve: {
    alias: {
        'vue$': 'vue/dist/vue.esm.js'
    }
}
};
